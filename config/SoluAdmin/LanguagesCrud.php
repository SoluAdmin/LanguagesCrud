<?php

return [
    'route_prefix' => '',
    'connection' => 'authorization',
    'user_model' => 'App\Models\Permission\User',
    'middleware' => false,
    'setup_routes' => false,
    'publishes_migrations' => false,
];
