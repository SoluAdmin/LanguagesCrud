@module("SoluAdmin\\LanguagesCrud")
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.LanguagesCrud.route_prefix' , '') . '/language') }}">
        <i class="fa fa-language"></i> <span>{{trans('SoluAdmin::LanguagesCrud.language_plural')}}</span>
    </a>
</li>
@endmodule