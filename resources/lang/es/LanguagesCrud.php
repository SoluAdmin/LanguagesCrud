<?php

return [
    'language_singular' => 'Idioma',
    'language_plural' => 'Idiomas',
    'name' => 'Nombre',
    'code' => 'Código',
];
