<?php

return [
    'language_singular' => 'Language',
    'language_plural' => 'Languages',
    'name' => 'Name',
    'code' => 'Code',
];
