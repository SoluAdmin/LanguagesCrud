<?php

namespace SoluAdmin\LanguagesCrud\Providers;

use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class LanguagesCrudServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::TRANSLATIONS,
        Assets::MIGRATIONS,
        Assets::VIEWS
    ];

    protected $resources = [
        'language' => "LanguageCrudController"
    ];
}
