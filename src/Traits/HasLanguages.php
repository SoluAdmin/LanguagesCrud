<?php

namespace SoluAdmin\LanguagesCrud\Traits;

use SoluAdmin\LanguagesCrud\Models\Language;

trait HasLanguages
{
    public function languages()
    {
        return $this->belongsToMany(
            Language::class,
            'language_users'
        );
    }
}
