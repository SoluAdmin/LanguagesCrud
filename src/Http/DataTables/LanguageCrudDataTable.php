<?php

namespace SoluAdmin\LanguagesCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class LanguageCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::LanguagesCrud.name'),
            ],
            [
                'name' => 'code',
                'label' => trans('SoluAdmin::LanguagesCrud.code'),
            ],
        ];
    }
}
