<?php

namespace SoluAdmin\LanguagesCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class LanguageCrudForm implements Form
{

    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::LanguagesCrud.name'),
            ],
            [
                'name' => 'code',
                'label' => trans('SoluAdmin::LanguagesCrud.code'),
            ],
        ];
    }
}
