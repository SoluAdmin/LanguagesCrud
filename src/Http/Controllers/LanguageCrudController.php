<?php

namespace SoluAdmin\LanguagesCrud\Http\Controllers;

use SoluAdmin\LanguagesCrud\Http\Requests\LanguageCrudRequest as StoreRequest;
use SoluAdmin\LanguagesCrud\Http\Requests\LanguageCrudRequest as UpdateRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class LanguageCrudController extends BaseCrudController
{

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
