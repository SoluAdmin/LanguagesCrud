<?php

namespace SoluAdmin\LanguagesCrud\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LanguageCrudRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
          'name' => 'required',
        ];
    }
}
