<?php

namespace SoluAdmin\LanguagesCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $translatable = ['name'];
    protected $connection = 'authorization';

    public function __construct(array $attributes = [])
    {
        $this->connection = config('SoluAdmin.LanguagesCrud.connection');
        parent::__construct($attributes);
    }

    public function users()
    {
        return $this->belongsToMany(
            config('SoluAdmin.LanguagesCrud.user_model'),
            'language_users'
        );
    }
}
